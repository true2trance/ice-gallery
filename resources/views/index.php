<!DOCTYPE html>
<html>
    <head>
        <title>Ingress Cell History</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
        <link rel="stylesheet" href="assets/vendor-bower/glidejs/dist/css/glide.core.min.css">
        <link rel="stylesheet" href="assets/vendor-bower/glidejs/dist/css/glide.theme.min.css">
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
        <script src="assets/vendor-bower/glidejs/dist/glide.js"></script>

        <script>

            var dates = <?php echo $dates; ?>

            $(function () {

                console.log(dates);

                var container = $('.ui-content');
//                
                for (var i in dates) {

                    var element = $('<option></option>');
                    element.data('items', dates[i]);
                    element.text(i + ' (' + dates[i].length + ' items)');
                    container.find('.dates').append(element);

                    container.find('.dates').on('change', function () {

                        var element = $(this).find(':selected');

                        container.find('.screenshots').empty();
                        var glide = container.find('.glide.template').clone();
                        glide.removeClass('template');

                        var glideTrack = glide.find('.glide__track');

                        element.data('items').forEach(function (item) {
                            var slide = $('<li class="glide__slide">');
                            var img = $('<h5 style="position: absolute; color: red; margin: 10px;">' + item.date + '</h5><img width="1920" style="display: block;">');
                            img.attr('src', '/ice/cache/' + item.hash + '.jpg');
                            slide.append(img)
                            glideTrack.prepend(slide);
                        });

                        container.find('.screenshots').append(glide);

                        glide.glide({
                            type: "slideshow",
                            animationDuration: 0,
                            autoplay: 250,
                            autoheight: true,
                        });

                    });
                }

//
//                files.forEach(function (file) {
//                    container.append('<img src="/' + file + '">');
//                });

            });

        </script>

        <style>

            .glide.template{
                display: none;
            }

        </style>

    </head>
    <body>
        <div data-role="page">

            <div data-role="header">
                <h1>Ingress Cell History</h1>
            </div>

            <div role="main" class="ui-content">

                <select class="dates">
                    <option selected="selected" disabled="disabled">Select Date</option>
                </select>

                <div class="screenshots" style="height: 500px;">

                </div>

                <div class="glide template">

                    <div class="glide__arrows">
                        <span class="glide__arrow prev" data-glide-dir="<">prev</span>
                        <span class="glide__arrow next" data-glide-dir=">">next</span>
                    </div>

                    <div class="glide__wrapper">
                        <ul class="glide__track">
                        </ul>
                    </div>

                </div>
            </div>

        </div>
    </body>
</html>
