<?php

namespace App\Http\Controllers;

use App\Screenshots;
use Illuminate\Support\Facades\DB;

class MainController extends Controller {

    public function getIndex() {

        $dates = Screenshots::orderBy('date', 'DESC')->get(['hash', 'date'])->groupBy(function($value) {
                    return substr($value->date, 0, 10);
                })->toJson();

        return view('index', [
            'dates' => $dates,
        ]);
    }

}
