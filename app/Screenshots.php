<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Screenshots extends Model {

    protected $table = 'screenshots';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename', 'hash', 'date'];
    public $timestamps = false;

}
