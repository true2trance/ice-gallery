<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Cache\Repository as CacheRepository;
use Illuminate\Filesystem\FilesystemManager;
use Carbon\Carbon;
use App\Screenshots;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Intervention\Image\Facades\Image;

class ProcessScreenshots extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:process-screenshots';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compress and sort raw screenshots';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FilesystemManager $fs, CacheRepository $cache) {
        parent::__construct();

        $this->fs = $fs;
        $this->cache = $cache;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $rawScreenShots = collect($this->fs->disk('public')->files('ice/raw'))
                ->filter(function($screenShot) {
            if (ends_with($screenShot, '.png')) {
                return true;
            }
        });

        $this->output->progressStart($rawScreenShots->count());

        $screenShots = $rawScreenShots->map(function($screenShot) {
            $this->output->progressAdvance();
            return [
                'path' => $screenShot,
                'hash' => $this->cache->rememberForever('FNAME:' . $screenShot . ':MD5', function() use ($screenShot) {
                            $md5 = md5($this->fs->disk('public')->get($screenShot));
                            return $md5;
                        })
            ];
        });

        $this->output->progressFinish();

        $this->output->progressStart($screenShots->count());

        $screenShots->each(function($screenshot) {

            $pathInfo = pathinfo($screenshot['path']);

            $screenShotDate = Carbon::createFromFormat('Y-m-d--H-i-s', substr($pathInfo['filename'], 4));

            try {
                Screenshots::where('hash', $screenshot['hash'])->firstOrFail();
                $this->fs->disk('public')->delete($screenshot['path']);
            } catch (ModelNotFoundException $exc) {
                try {
                    $image = Image::make(public_path($screenshot['path']));
                    $image->save(public_path('ice/cache/' . $screenshot['hash'] . '.jpg', 70));
                    $image->destroy();
                    $this->fs->disk('public')->delete($screenshot['path']);
                } catch (\Exception $ex) {
                    $this->output->error($ex->getMessage());
                }
            }


            try {
                Screenshots::create([
                    'hash' => $screenshot['hash'],
                    'date' => $screenShotDate
                ]);
            } catch (\Exception $ex) {
                
            }

            $this->output->progressAdvance();

            $this->info(' Used: ' . memory_get_usage() / 1024 / 1024 . 'MB');
        });


        $this->output->progressFinish();
//        dd($screenShots);
    }

}
